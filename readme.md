# Sofware haus task
## in order to see final result please do next steps 

1. create new empty DIR inside your local enviorment ( and execute all code )
2. pull code from repositroy http link is "https://danicicPredrag@bitbucket.org/danicicPredrag/software_haus.git"
3. Create database
4. copy .env.example in  new .env file , and set all needed variables ( especially MAIL_* and DATABASE_* variables)
5. run composer update to pull all vendor files 

### Create database ( if anything fails here just execute command "run php artisan db:wipe")
5. run php artisan migrate
6. run php artisan db:seed

7. Go to http://localhost:8000/ and you should see list of users ... 

8. To create new job posting go to http://localhost:8000/{user_id}/create_new_job
9. After job creation you should recive email with link to approve it or to mark it as spamm 
10. All job postings you can see on http://localhost:8000/all_jobs 