<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class JovReview extends Mailable

{

    use Queueable, SerializesModels;

    public function __construct($link)

    {

        $this->link = $link;

    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()

    {

        return $this->view('emails.HDTutoMail');

    }

}