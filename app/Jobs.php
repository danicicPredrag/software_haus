<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * Class Jobs
 * @package App
 * @property int $user_id;
 * @property string $title
 * @property string $email
 * @property string $token = str_random(200);
 * @property string $description
 * @property int $job_status
 */
class Jobs extends Model
{

    const TABLE_NAME = 'jobs';
    const PRIMARY_KEY = 'job_id';

    const JOB_STATUS_PENDING = 1;
    const JOB_STATUS_APPROVED = 2;
    const JOB_STATUS_SPAM = 3;

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:M',
        'updated_at' => 'datetime:Y-m-d H:M',
    ];

    const VALIDATION_RULES_REGISTER = [
        'email' => ['required', 'email', 'max:255', 'exists:users,email',],
        'title' => ['required', 'string', 'max:255', 'min:10',],
        'description' => ['required', 'string', 'min:150',],
    ];

    protected $primaryKey = Jobs::PRIMARY_KEY;

    public function user()
    {
        return $this->belongsTo(Users::class, Users::TABLE_NAME, Users::PRIMARY_KEY, '');
    }
}