<?php

namespace App\Http\Controllers;

use App\Jobs;
use App\Users;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class JobsController extends Controller
{
    public function displayNewJobFormAction($user_id)
    {
        $user = Users::whereUserId($user_id)->get()->first();
        if ($user->user_level != Users::USER_LEVEL_HR_MANAGER) {
            dd('only HR managers can posts new jobs');
        }

        return view('pages.new_job_page')->with(['user_id' => $user_id]);
    }

    public function createNewJobAction($user_id, Request $request)
    {
        $validator = Validator::make($request->all(), Jobs::VALIDATION_RULES_REGISTER);
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->getMessageBag());
        };

        // Usually I want to have all validation rules inside constants of models.
        // However sometimes because of that I have do double validation and do second query to database.
        // I know it is not most effective way of doing things but I like clear code.
        $user_level_board_moderator = Users::USER_LEVEL_BOARD_MODERATOR;
        $validator = Validator::make($request->all(), [
                'email' => [
                'required',
                Rule::exists(Users::TABLE_NAME)->where(function ($query) use ($user_level_board_moderator) {
                        $query->where('user_level', $user_level_board_moderator);
                    })
                ],
            ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->getMessageBag());
        };

        $job_status_approved = Jobs::JOB_STATUS_APPROVED;

        $user = Users::whereUserId($user_id)->with(['jobs' => function ($query) use ($job_status_approved) {
            $query->where('job_status', '=', $job_status_approved);
        }])->get()->first();
        $job_status = Jobs::JOB_STATUS_APPROVED;

        if (empty($user->jobs)) {
            $job_status = Jobs::JOB_STATUS_PENDING;
        }

        $new_job = new Jobs();
        $new_job->user_id = $user_id;
        $new_job->title = $request->title;
        $new_job->email = $request->email;
        $new_job->token = str_random(200);
        $new_job->description = $request->description;
        $new_job->job_status = $job_status;
        $new_job->save();

        if (empty($new_job->job_id)) {
            dd('job not created error inserting into database throw exception');
        }

        if ($job_status == Jobs::JOB_STATUS_APPROVED){
            echo "job status is posted and it is aproved automaticly";
            die();
        }

        $data['link'] = route('verify-new-job-get', [
            'job_id' => $new_job->job_id,
            'token' => $new_job->token,
        ]);

        $mail = $request->email;
        Mail::send(['html' => 'mails.job_created_mail'], $data, function ($message) use ($mail){
            $message->to($mail, 'Software Haus task Predrag')->subject('New Job created');
            $message->from(config('mail.username'), config('app.name'));
        });

        echo "email sent check your inbox";
    }

    public function displayPendingJobAction($job_id, $token)
    {
        $job = Jobs::where(Jobs::PRIMARY_KEY, $job_id)->where('token',$token)->get()->first();
        if (empty($job)) {
            dd('job not found');
        }

        return view('pages.new_job_page')->with(['job_data' => $job]);
    }

    public function approveJobAction($job_id, $token) {
        $job = Jobs::where(Jobs::PRIMARY_KEY, $job_id)->where('token',$token)->get()->first();
        if (empty($job)) {
            dd('job not found');
        }
        $job->job_status = Jobs::JOB_STATUS_APPROVED;
        $job->token = '';
        $job->save();
        echo "job approved token removed go to <a href=" . route('all-jobs') ."> link </a> to see result";
    }

    public function markAsSpamAction($job_id, $token) {
        $job = Jobs::where(Jobs::PRIMARY_KEY, $job_id)->where('token',$token)->get()->first();
        if (empty($job)) {
            dd('job not found');
        }
        $job->job_status = Jobs::JOB_STATUS_SPAM;
        $job->token = '';
        $job->save();
        echo "job approved token removed go to <a href=" . route('all-jobs') ."> link </a> to see result";
    }

    public function listAllJobsAction() {
        $job_list = Jobs::select()->paginate(10);
        return view('pages.job_list')->with(['job_list' => $job_list]);
    }
}
