<?php

namespace App\Http\Controllers;
use App\Jobs;
use App\Users;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{

     public function listAllUsersAction () {

         $user_list = Users::withCount('jobs')->paginate(10);
         return view('pages.user_list')->with(['user_list' => $user_list]);
     }
}