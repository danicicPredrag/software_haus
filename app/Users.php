<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    const TABLE_NAME = 'users';
    const PRIMARY_KEY = 'user_id';

    const USER_LEVEL_HR_MANAGER = 1;
    const USER_LEVEL_BOARD_MODERATOR = 2;

    protected $primaryKey = Jobs::PRIMARY_KEY;

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:M',
        'updated_at' => 'datetime:Y-m-d H:M',
    ];

    public function jobs()
    {
        return $this->hasMany(Jobs::class, Users::PRIMARY_KEY, Users::PRIMARY_KEY);
    }
}