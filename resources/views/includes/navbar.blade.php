<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Software Haus Zadatak</a>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="{{route('all-jobs')}}">Job postings </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('all-users')}}">Users</a>
        </li>
    </ul>
</nav>
@include('includes.flash-messages')