<div class='container'>
    <div class='row justify-content-center'>
        <div class='col-md-8'>
            <div class='card'>
                <div class='card-header text-center'>Create new job</div>

                <div class='card-body'>
                    <form method='POST' action="{{route('create-new-job-post', $user_id ) }}">
                        @csrf

                        <div class='form-group row'>
                            <label for='title' class='col-sm-4 col-form-label text-md-right'>Job title:</label>

                            <div class='col-md-6'>
                                <input id='title' type='title'
                                       class='form-control{{ $errors->has('title') ? ' is-invalid' : '' }}' name='title'
                                       value='{{ old('title') }}' required autofocus/>

                                @if ($errors->has('title'))
                                    <span class='invalid-feedback'>
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class='form-group row'>
                            <label for='email' class='col-sm-4 col-form-label text-md-right'>Email of Board Manager (look manually in database):</label>

                            <div class='col-md-6'>
                                <input id='email' type='email'
                                       class='form-control{{ $errors->has('email') ? ' is-invalid' : '' }}' name='email'
                                       value='{{ old('email') }}' required autofocus/>

                                @if ($errors->has('email'))
                                    <span class='invalid-feedback'>
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class='form-group row'>
                            <label for='description'
                                   class='col-sm-4 col-form-label text-md-right'>Description :</label>

                            <div class='col-md-6'>
                                <textarea id='description'
                                       class='form-control{{ $errors->has('description') ? ' is-invalid' : '' }}'
                                          name='description' required autofocus >{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <span class='invalid-feedback'>
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class='form-group row mb-0'>
                            <div class='col-md-6 offset-md-4'>
                                <button type='submit' class='btn btn-primary'>Create new Job</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>