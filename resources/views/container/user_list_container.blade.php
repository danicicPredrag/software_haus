<div class='container'>
    <table class="table">
        <thead>
        <tr>
            <th>user id</th>
            <th>name</th>
            <th>email</th>
            <th>level</th>
            <th>Jobs Count</th>
            <th>created_at</th>
            <th>updated_at</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($user_list as $user)
            <tr>
                <td>{{ $user->user_id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->user_level }}</td>
                <td>{{ $user->jobs_count }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->updaterd_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $user_list->links() }}
</div>