<div class='container'>
    <div class='row justify-content-center'>
        <div class='col-md-8'>
            Job title: {{$job_data->title}}
        </div>
    </div>
    <br><br><br>
    <div class='row justify-content-center'>
        <div class='col-md-8'>
            Job description: {{$job_data->description}}
        </div>
    </div>

    <div class='row justify-content-center'>
        <div class='col-md-8'>
            <form method='POST' action="{{route('job-approve', ['token'=> $job_data->token, 'job_id'=> $job_data->job_id] ) }}">
                @csrf
                <div class='form-group row mb-0'>
                    <div class='col-md-6 offset-md-4'>
                        <button type='submit' class='btn btn-primary'>Approve</button>
                    </div>
                </div>
            </form>
            <br><br><br>
            <form method='POST' action="{{route('job-mark-as-spam', ['token'=> $job_data->token, 'job_id'=> $job_data->job_id] ) }}">
                @csrf
                <div class='form-group row mb-0'>
                    <div class='col-md-6 offset-md-4'>
                        <button type='submit' class='btn btn-danger'>Mark as spam</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>