<div class='container'>
    <table>
        <tr>
            <th>job id</th>
            <th>title</th>
            <th>job_status</th>
        </tr>

        @foreach ($job_list as $job)
            <tr>
                <td>{{ $job->job_id }}</td>
                <td>{{ $job->title }}</td>
                <td>{{ $job->job_status }}</td>
            </tr>
        @endforeach

    </table>

    {{ $job_list->links() }}
</div>