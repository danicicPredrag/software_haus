<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'UsersController@listAllUsersAction')->name('all-users');

Route::prefix('{user_id}')->group(function () {
    Route::get('/create_new_job', 'JobsController@displayNewJobFormAction')->name('create-new-job-get');
    Route::post('/create_new_job', 'JobsController@createNewJobAction')->name('create-new-job-post');
});

Route::get('/verify_new_job/{job_id}/{token}', 'JobsController@displayPendingJobAction')->name('verify-new-job-get');
Route::post('/approve_job/{job_id}/{token}', 'JobsController@approveJobAction')->name('job-approve');
Route::post('/mark_as_spam/{job_id}/{token}', 'JobsController@markAsSpamAction')->name('job-mark-as-spam');

Route::get('all_jobs', 'JobsController@listAllJobsAction')->name('all-jobs');
Route::get('all_users', 'UsersController@listAllUsersAction')->name('all-users');