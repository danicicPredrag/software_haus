<?php

use App\Users;
use App\Jobs;
use Symfony\Component\Console\Helper\Helper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class JobsTableSeeder extends Seeder
{

    public function run()
    {

        $faker = Faker::create();


        for ($i = 0; $i < 20; $i++) {
            $user = DB::select("SELECT * FROM `" . Users::TABLE_NAME . "` ORDER BY RAND() LIMIT 1");
            DB::table(Jobs::TABLE_NAME)->insert([
                'user_id' => $user[0]->user_id,
                'title' => $faker->text(150),
                'email' => $faker->email,
                'description' => $faker->text(1500),
                'job_status' => rand(1,3),
                'token' => str_random(200),
            ]);
        }
    }
}