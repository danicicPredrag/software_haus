<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Users;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{

    public function run()
    {

        $faker = Faker::create();
        for ($i = 0; $i < 20; $i++) {
            DB::table(Users::TABLE_NAME)->insert([
                'name' => $faker->name(),
                'email' => $faker->email,
                'user_level' => rand(1, 2),
                'password' => str_random(15)
            ]);
        }
    }
}